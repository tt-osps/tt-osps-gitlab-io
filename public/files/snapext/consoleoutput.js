SnapExtensions.primitives.set('consoleLog(input)', function(input) {
    console.log(input);
  });
SnapExtensions.primitives.set('consoleWarn(input)', function(input) {
    console.warn(input);
  });
SnapExtensions.primitives.set('consoleError(input)', function(input) {
    console.error(input);
});
  SnapExtensions.primitives.set('consoleDebug(input)', function(input) {
    console.debug(input);
});
  SnapExtensions.primitives.set('ClearConsole', function() {
    console.clear();
});
  